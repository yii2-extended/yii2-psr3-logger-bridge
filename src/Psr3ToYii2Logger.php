<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-psr3-logger-bridge library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Extended\Yii2Log;

use DateInterval;
use DateTime;
use DateTimeInterface;
use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;
use Stringable;
use yii\BaseYii;
use yii\log\Logger;

/**
 * Psr3ToYii2Logger class file.
 * 
 * This class transforms psr-3 compliant log messages to yii2 compliant log
 * messages.
 * 
 * @author Anastaszor
 */
class Psr3ToYii2Logger extends AbstractLogger implements Stringable
{
	
	public const CONTEXT_LOG_CATEGORY_KEY = '__log_category__';
	public const CONTEXT_T_CATEGORY_KEY = '__t_category__';
	public const CONTEXT_LANGUAGE_KEY = '__language__';
	
	/**
	 * @var string
	 */
	public string $defaultTranslationCategory = 'yii';
	
	/**
	 * A default category to apply to all log records if their category is not
	 * defined.
	 * 
	 * @var string
	 */
	public string $defaultLogCategory = 'application';
	
	/**
	 * A default language to apply to all log records if their language is not
	 * defined.
	 * 
	 * @var ?string
	 */
	public ?string $defaultLanguage = null;
	
	/**
	 * The conversion table between the psr3 log levels and yii log levels.
	 * 
	 * @var array<string|integer, integer>
	 */
	protected array $_logLevelMap = [
		// psr-3 log levels
		LogLevel::EMERGENCY => Logger::LEVEL_ERROR,
		LogLevel::ALERT => Logger::LEVEL_ERROR,
		LogLevel::CRITICAL => Logger::LEVEL_ERROR,
		LogLevel::ERROR => Logger::LEVEL_ERROR,
		LogLevel::WARNING => Logger::LEVEL_WARNING,
		LogLevel::NOTICE => Logger::LEVEL_INFO,
		LogLevel::INFO => Logger::LEVEL_INFO,
		LogLevel::DEBUG => Logger::LEVEL_TRACE,
		
		// yii2 log levels
		Logger::LEVEL_ERROR => Logger::LEVEL_ERROR,
		Logger::LEVEL_WARNING => Logger::LEVEL_WARNING,
		Logger::LEVEL_INFO => Logger::LEVEL_INFO,
		Logger::LEVEL_TRACE => Logger::LEVEL_TRACE,
		Logger::LEVEL_PROFILE => Logger::LEVEL_PROFILE,
		Logger::LEVEL_PROFILE_BEGIN => Logger::LEVEL_PROFILE_BEGIN,
		Logger::LEVEL_PROFILE_END => Logger::LEVEL_PROFILE_END,
	];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Log\LoggerInterface::log()
	 * @param int|string $level
	 * @param array<string, null|bool|int|float|string|array<int|string, null|bool|int|float|string>> $context
	 * @psalm-suppress MoreSpecificImplementedParamType
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function log($level, string|Stringable $message, array $context = []) : void
	{
		$level = $this->getStrval($level);
		$message = (string) $message;
		
		$logCategory = $this->defaultLogCategory;
		if(isset($context[self::CONTEXT_LOG_CATEGORY_KEY]))
		{
			$logCategory = $this->getStrval($context[self::CONTEXT_LOG_CATEGORY_KEY]);
			unset($context[self::CONTEXT_LOG_CATEGORY_KEY]);
		}
		
		$tCategory = $this->defaultTranslationCategory;
		if(isset($context[self::CONTEXT_T_CATEGORY_KEY]))
		{
			$tCategory = $this->getStrval($context[self::CONTEXT_T_CATEGORY_KEY]);
			unset($context[self::CONTEXT_T_CATEGORY_KEY]);
		}
		
		$language = $this->defaultLanguage;
		if(isset($context[self::CONTEXT_LANGUAGE_KEY]))
		{
			$language = $this->getStrval($context[self::CONTEXT_LANGUAGE_KEY]);
			unset($context[self::CONTEXT_LANGUAGE_KEY]);
		}
		
		$newlevel = Logger::LEVEL_ERROR;
		if(isset($this->_logLevelMap[$level]))
		{
			$newlevel = $this->_logLevelMap[$level];
		}
		
		// strtr in Yii::t() does not accept values others than strings
		// so we have to preprocess the context to ensure that all is strings
		/** @var array<string, string> $newctx */
		$newctx = [];
		
		foreach($context as $key => $value)
		{
			$newctx[$key] = $this->getStrval($value);
		}
		
		$tmessage = BaseYii::t($tCategory, $message, $newctx, $language);
		BaseYii::getLogger()->log($tmessage, $newlevel, $logCategory);
	}
	
	/**
	 * Gets a string from the given value.
	 * 
	 * @param null|boolean|integer|float|string|object|resource|array<integer|string, null|boolean|integer|float|string|object|resource|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param integer $indent
	 * @return string
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	protected function getStrval($value, int $indent = 1) : string
	{
		if(\is_resource($value))
		{
			return 'resource('.\get_resource_type($value).')';
		}
		
		if(\is_object($value))
		{
			if($value instanceof Stringable)
			{
				return (string) $value;
			}
			
			if($value instanceof DateTimeInterface)
			{
				return $value->format(DateTime::RFC3339_EXTENDED);
			}
			
			if($value instanceof DateInterval)
			{
				return $value->format('Interval: %R %Y years, %M months, %D days, %H hours, %I minutes, %S seconds, %F microseconds');
			}
			
			if(\method_exists($value, '__toString'))
			{
				return \get_class($value).'('.((string) $value->__toString()).')';
			}
			
			return \get_class($value).'('.\serialize($value).')';
		}
		
		if(null === $value)
		{
			return 'null';
		}
		
		if(\is_bool($value))
		{
			return $value ? 'true' : 'false';
		}
		
		if(\is_array($value))
		{
			if(0 === \count($value))
			{
				return '[]';
			}
			if(1 === \count($value) && isset($value[0]))
			{
				return '['.$this->getStrval($value[0]).']';
			}
			$strval = "[\n";
			
			foreach($value as $key => $value)
			{
				$keyStr = \is_string($key) ? '"'.$key.'"' : (string) $key;
				$strval .= \str_repeat("\t", $indent).$keyStr.' => '.$this->getStrval($value, $indent + 1)."\n";
			}
			
			return $strval.\str_repeat("\t", (int) \max(0, $indent - 1)).']';
		}
		
		return (string) $value;
	}
	
}
