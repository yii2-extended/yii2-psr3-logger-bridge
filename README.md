# yii2-extended/yii2-psr3-logger-bridge
A library to bridge psr-3 loggers with the yii logging system

![coverage](https://gitlab.com/yii2-extended/yii2-psr3-logger-bridge/badges/master/pipeline.svg?style=flat-square) 
![build status](https://gitlab.com/yii2-extended/yii2-psr3-logger-bridge/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar yii2-extended/yii2-psr3-logger-bridge ^8`


## Basic Usage

This library offers one psr3 compliant logger to log messages back to yii2 
logging system. As Yii2 has a limited number of logging levels, this logger
will attempt to use the closest Yii2 equivalent for the provided psr3 level.

If you are searching for the reverse (yii2 logger that logs to psr3 compliant
targets, look at the official [yii2-psr-log-target](https://github.com/samdark/yii2-psr-log-target).

The `Psr3ToYii2Logger` is straightforward to use, just look at the
[PSR-3 documentation](http://www.php-fig.org/psr/psr-3/).

Any message that is logged by this logger will be transmitted to Yii's
application `log` component.

```php

use Yii2Extended\Yii2Log\Psr3ToYii2Logger;
use Psr\Log\LogLevel;
use yii\log\Logger;

$logger = new Psr3ToYii2Logger();
// with the psr3 methods
$logger->emergency('A psr3 emergency message, logged as Yii2 error message');
// with the psr3 log levels
$logger->log(LogLevel::ALERT, 'A psr3 alert message, logged as Yii2 error message');
// with the yii2 log levels
$logger->log(Logger::LEVEL_PROFILE_BEGIN, 'A yii2 profile begin message, logged as profile begin message');

```

Note that the messages that are passed through this component may use optionals
`__category__` and `__language__` keys within the `$context` array. This
will be useful to orient the category of the logging message, and the language
in which it should be translated, in case of need (as the translation to yii
interpolates the message with the context and tries to translate it, using the
`Yii::t()` method).


## License

MIT (See [license file](LICENSE)).
